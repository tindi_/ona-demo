import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 19/05/2017.
 */
public class WaterPoints {

    @SerializedName("water_pay")
    @Expose
    private String waterPay;
    @SerializedName("respondent")
    @Expose
    private String respondent;
    @SerializedName("research_asst_name")
    @Expose
    private String researchAsstName;
    @SerializedName("water_used_season")
    @Expose
    private String waterUsedSeason;
    @SerializedName("_bamboo_dataset_id")
    @Expose
    private String bambooDatasetId;
    @SerializedName("_deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("water_point_condition")
    @Expose
    private String waterPointCondition;
    @SerializedName("_xform_id_string")
    @Expose
    private String xformIdString;
    @SerializedName("other_point_1km")
    @Expose
    private String otherPoint1km;
    @SerializedName("_attachments")
    @Expose
    private List<String> attachments = null;
    @SerializedName("communities_villages")
    @Expose
    private String communitiesVillages;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("animal_number")
    @Expose
    private String animalNumber;
    @SerializedName("water_point_id")
    @Expose
    private String waterPointId;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("water_connected")
    @Expose
    private String waterConnected;
    @SerializedName("water_manager_name")
    @Expose
    private String waterManagerName;
    @SerializedName("_status")
    @Expose
    private String status;
    @SerializedName("enum_id_1")
    @Expose
    private String enumId1;
    @SerializedName("water_lift_mechanism")
    @Expose
    private String waterLiftMechanism;
    @SerializedName("districts_divisions")
    @Expose
    private String districtsDivisions;
    @SerializedName("_uuid")
    @Expose
    private String uuid;
    @SerializedName("grid")
    @Expose
    private String grid;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("formhub/uuid")
    @Expose
    private String formhubUuid;
    @SerializedName("road_available")
    @Expose
    private String roadAvailable;
    @SerializedName("water_functioning")
    @Expose
    private String waterFunctioning;
    @SerializedName("_submission_time")
    @Expose
    private String submissionTime;
    @SerializedName("signal")
    @Expose
    private String signal;
    @SerializedName("water_source_type")
    @Expose
    private String waterSourceType;
    @SerializedName("_geolocation")
    @Expose
    private List<String> geolocation = null;
    @SerializedName("water_point_image")
    @Expose
    private String waterPointImage;
    @SerializedName("water_point_geocode")
    @Expose
    private String waterPointGeocode;
    @SerializedName("deviceid")
    @Expose
    private String deviceid;
    @SerializedName("locations_wards")
    @Expose
    private String locationsWards;
    @SerializedName("water_manager")
    @Expose
    private String waterManager;
    @SerializedName("water_developer")
    @Expose
    private String waterDeveloper;
    @SerializedName("_id")
    @Expose
    private Integer id;
    @SerializedName("animal_point")
    @Expose
    private String animalPoint;

    public String getWaterPay() {
        return waterPay;
    }

    public void setWaterPay(String waterPay) {
        this.waterPay = waterPay;
    }

    public String getRespondent() {
        return respondent;
    }

    public void setRespondent(String respondent) {
        this.respondent = respondent;
    }

    public String getResearchAsstName() {
        return researchAsstName;
    }

    public void setResearchAsstName(String researchAsstName) {
        this.researchAsstName = researchAsstName;
    }

    public String getWaterUsedSeason() {
        return waterUsedSeason;
    }

    public void setWaterUsedSeason(String waterUsedSeason) {
        this.waterUsedSeason = waterUsedSeason;
    }

    public String getBambooDatasetId() {
        return bambooDatasetId;
    }

    public void setBambooDatasetId(String bambooDatasetId) {
        this.bambooDatasetId = bambooDatasetId;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getWaterPointCondition() {
        return waterPointCondition;
    }

    public void setWaterPointCondition(String waterPointCondition) {
        this.waterPointCondition = waterPointCondition;
    }

    public String getXformIdString() {
        return xformIdString;
    }

    public void setXformIdString(String xformIdString) {
        this.xformIdString = xformIdString;
    }

    public String getOtherPoint1km() {
        return otherPoint1km;
    }

    public void setOtherPoint1km(String otherPoint1km) {
        this.otherPoint1km = otherPoint1km;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public String getCommunitiesVillages() {
        return communitiesVillages;
    }

    public void setCommunitiesVillages(String communitiesVillages) {
        this.communitiesVillages = communitiesVillages;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getAnimalNumber() {
        return animalNumber;
    }

    public void setAnimalNumber(String animalNumber) {
        this.animalNumber = animalNumber;
    }

    public String getWaterPointId() {
        return waterPointId;
    }

    public void setWaterPointId(String waterPointId) {
        this.waterPointId = waterPointId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getWaterConnected() {
        return waterConnected;
    }

    public void setWaterConnected(String waterConnected) {
        this.waterConnected = waterConnected;
    }

    public String getWaterManagerName() {
        return waterManagerName;
    }

    public void setWaterManagerName(String waterManagerName) {
        this.waterManagerName = waterManagerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnumId1() {
        return enumId1;
    }

    public void setEnumId1(String enumId1) {
        this.enumId1 = enumId1;
    }

    public String getWaterLiftMechanism() {
        return waterLiftMechanism;
    }

    public void setWaterLiftMechanism(String waterLiftMechanism) {
        this.waterLiftMechanism = waterLiftMechanism;
    }

    public String getDistrictsDivisions() {
        return districtsDivisions;
    }

    public void setDistrictsDivisions(String districtsDivisions) {
        this.districtsDivisions = districtsDivisions;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFormhubUuid() {
        return formhubUuid;
    }

    public void setFormhubUuid(String formhubUuid) {
        this.formhubUuid = formhubUuid;
    }

    public String getRoadAvailable() {
        return roadAvailable;
    }

    public void setRoadAvailable(String roadAvailable) {
        this.roadAvailable = roadAvailable;
    }

    public String getWaterFunctioning() {
        return waterFunctioning;
    }

    public void setWaterFunctioning(String waterFunctioning) {
        this.waterFunctioning = waterFunctioning;
    }

    public String getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(String submissionTime) {
        this.submissionTime = submissionTime;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public String getWaterSourceType() {
        return waterSourceType;
    }

    public void setWaterSourceType(String waterSourceType) {
        this.waterSourceType = waterSourceType;
    }

    public List<String> getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(List<String> geolocation) {
        this.geolocation = geolocation;
    }

    public String getWaterPointImage() {
        return waterPointImage;
    }

    public void setWaterPointImage(String waterPointImage) {
        this.waterPointImage = waterPointImage;
    }

    public String getWaterPointGeocode() {
        return waterPointGeocode;
    }

    public void setWaterPointGeocode(String waterPointGeocode) {
        this.waterPointGeocode = waterPointGeocode;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getLocationsWards() {
        return locationsWards;
    }

    public void setLocationsWards(String locationsWards) {
        this.locationsWards = locationsWards;
    }

    public String getWaterManager() {
        return waterManager;
    }

    public void setWaterManager(String waterManager) {
        this.waterManager = waterManager;
    }

    public String getWaterDeveloper() {
        return waterDeveloper;
    }

    public void setWaterDeveloper(String waterDeveloper) {
        this.waterDeveloper = waterDeveloper;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnimalPoint() {
        return animalPoint;
    }

    public void setAnimalPoint(String animalPoint) {
        this.animalPoint = animalPoint;
    }


}
