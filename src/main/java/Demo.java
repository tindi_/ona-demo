import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by admin on 19/05/2017.
 */
public class Demo {

    static ArrayList<String> functioning = new ArrayList<>();
    static ArrayList<String> broken = new ArrayList<>();
    static ArrayList<String> community = new ArrayList<>();

    static List<WaterPoints> points = new List<WaterPoints>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<WaterPoints> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(WaterPoints waterPoints) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends WaterPoints> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends WaterPoints> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public WaterPoints get(int index) {
            return null;
        }

        @Override
        public WaterPoints set(int index, WaterPoints element) {
            return null;
        }

        @Override
        public void add(int index, WaterPoints element) {

        }

        @Override
        public WaterPoints remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<WaterPoints> listIterator() {
            return null;
        }

        @Override
        public ListIterator<WaterPoints> listIterator(int index) {
            return null;
        }

        @Override
        public List<WaterPoints> subList(int fromIndex, int toIndex) {
            return null;
        }
    };

    public static void main(String[] args) {
        calculate();
    }


    private static void calculate() {
        Api api = Api.getInstance();
        api.setIsDebug(true);

        Call<List<WaterPoints>> call = api.getService().get();
        call.enqueue(new Callback<List<WaterPoints>>() {
            @Override
            public void onResponse(Call<List<WaterPoints>> call, Response<List<WaterPoints>> response) {

                System.out.println("Response status code: " + response.code());
                System.out.println(response.body());

                points = response.body();

                if(points!=null){
                    for (WaterPoints p: points) {

                        // Functioning Water Points
                        if (p.getWaterPointCondition().equals("functioning")) {
                            functioning.add(p.getWaterPointCondition());
                        }

                        // Broken Water Points
                        if (p.getWaterPointCondition().equals("broken")) {
                            broken.add(p.getWaterPointCondition());

                        }

                        // Community Villages
                        community.add(p.getCommunitiesVillages());

                    }
                }

                // VILLAGE AND NUMBER OF WATER POINTS

                Map<String, Long> uniquePoints = points.stream()
                        .collect(Collectors.groupingBy(WaterPoints::getCommunitiesVillages, Collectors.counting()));
                System.out.print("Water Points Per Village " + uniquePoints +"\n");

                uniquePoints.entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue())
                        .forEach(System.out::println);



                // RANK OF COMMUNITY BY PERCENTAGE OF BROKEN WATER POINTS
                Map<String, Long> all = points.stream()
                        .collect(Collectors.groupingBy(WaterPoints::getCommunitiesVillages, Collectors.counting()));
                System.out.print("ALL WATER POINTS" + all + "\n");

                Map<String, Long> endGame = new HashMap<>();

                points.stream()
                        .filter(waterPoints -> waterPoints.getWaterPointCondition().equals("broken"))
                        .collect(Collectors.groupingBy(foo -> foo.getCommunitiesVillages(), Collectors.counting()))
                        .forEach((village, totalBroken) ->  endGame.put(village, totalBroken *100/all.get(village)));

                System.out.print(endGame+"\n");

                Map<String, Long> result = new LinkedHashMap<>();
                endGame.entrySet().stream()
                        .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                        .forEachOrdered(x -> result.put(x.getKey(), x.getValue()));


                JsonObject numberPoints = new JsonObject();
                uniquePoints.forEach((k,v) -> numberPoints.addProperty(k, v));

                JsonObject funcObj = new JsonObject();
                funcObj.addProperty("number_functional", functioning.size());

                JsonObject rankObj = new JsonObject();
                result.forEach((j,k) -> rankObj.addProperty(j, k+"%"));

                JsonObject mainObj = new JsonObject();
                mainObj.add("", funcObj);
                mainObj.add("number_water_points", numberPoints);
                mainObj.add("community_ranking_descending", rankObj);

                System.out.print(mainObj);


            }

            @Override
            public void onFailure(Call<List<WaterPoints>> call, Throwable t) {
                System.out.println("onFailure");
                System.out.println(t.getMessage());
            }
        });

    }
}
