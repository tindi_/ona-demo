
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.*;

import java.util.List;

/**
 * Created by admin on 19/05/2017.
 */
public class Api {

    public static final String API_URL = "https://raw.githubusercontent.com/onaio/ona-tech/master/data/";

    private static Api instance = null;
    private HttpService service;
    private boolean isDebug;
    private HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    private static Retrofit retrofit = null;

    public interface HttpService {
        @GET("water_points.json")
        Call<List<WaterPoints>> get();

    }

    /**
     * Private constructor
     */
    private Api() {
        // Http interceptor to add custom headers to every request
        OkHttpClient httpClient = new OkHttpClient();

        // Retrofit setup
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Service setup
        service = retrofit.create(HttpService.class);
    }

    public Api setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
        if (retrofit != null) {
            httpLoggingInterceptor.
                    setLevel(isDebug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        }
        return this;
    }

    /**
     * Get the HttpApi singleton instance
     */
    public static Api getInstance() {
        if(instance == null) {
            instance = new Api();
        }
        return instance;
    }

    /**
     * Get the API service to execute calls with
     */
    public HttpService getService() {
        return service;
    }



}
